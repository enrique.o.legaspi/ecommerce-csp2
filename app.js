// [SECTION]Package and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv");
const orderRoutes = require('./routes/orders');
const productRoutes = require('./routes/products');
const userRoutes = require('./routes/users');

// [SECTION]Server Setup
const app = express();
app.use(express.json());
dotenv.config();
app.use(cors())
const secret = process.env.CONNECTION_STRING;
const port = process.env.PORT;

// [SECTION] Application Routes

app.use('/orders', orderRoutes);
app.use('/products', productRoutes);
app.use('/users', userRoutes);
// [SECTION] Database Connect
mongoose.connect(secret);
let connectStatus = mongoose.connection;
connectStatus.once('open', () => console.log(`Database is connected`));

// [SECTION]Gateway Response
app.get('/', (req, res) => {
     res.send(`Welcome to Welcome to Keta's Online Store`); 
});

app.listen(port, () => console.log(`Server is running on port ${port}`));