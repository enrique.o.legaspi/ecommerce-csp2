//[SECTION] Dependencies and Modules
	const Order = require('../models/order');
	const User = require('./../models/user');
	const Product = require('./../models/product');
	const bcrypt = require("bcrypt");

// [SECTION] MAKE Order
module.exports.makeOrder = async (req, res) => {
	let userId = req.user.id;
	let prodId = req.body.productId;
	let ordQty = req.body.quantity;
	let prodPrice = req.body.price;

	if (req.user.isAdmin) {
		res.send("Action Forbidden.");
	}

	const prodInfo = Product.findById(prodId).then(result => {
		return result;
	})
	console.log(prodInfo);

	// Product.findById(prodId).then(data => {
	// 	console.log(data.productName);
	// 	console.log(data.price)
	// 	if (data) {
	// 		return prodPrice = data.price;
	// 	} else {
	// 		return res.send('Unable to find product. Make sure you have selected the correct item.');
	// 	}
	// 	console.log(prodPrice);
	// })
	// console.log(prodPrice);

	let totAmt = ordQty * prodPrice;
	console.log(totAmt);

	let newOrder = new Order ({
			totalAmount: totAmt,
			customerId: userId,
		})

	newOrder.save().then((savedOrder, error) => {
		if (error) {
			res.send('An error occured. Failed to checkout order.');
		} else {
			let isUserUpdated = User.findById(userId).then(user => {
				let orderId = savedOrder.id;
				let nOrder = {					
						orderId: orderId,
						orderTotal: totAmt
					};

				user.orders.push(nOrder);
				return user.save().then(user => true).catch(err => err.message)
			})

			if(isUserUpdated !== true){
		        return res.send("message: isUserUpdated")
		    }

		    let isProductUpdate = Product.findById(prodId).then(product => {
		    	let oldQty = product.quantity;
		    	let newQty = oldQty - ordQty;

		    	let nQty = {
		    			quantity: newQty
		    	};
		    	product.quantity.update(newQty);

		    	let pOrder = {
		    			orderId: orderId,
		    			orderQty: ordQty
		    	}

		    	product.orders.push(pOrder);
		    	return product.save().then(product => true).catch(err => err.message)
		    })

		    if (isProductUpdate !== true){
		    	return res.send({message: "Order Checked Our Succesfully."})
		    }

			// let orderId = savedOrder.id;
			// let updateUser = {
			// 	orders: [
			// 		{
			// 			orderId: orderId,
			// 			orderTotal: totAmt
			// 		}
			// 	]
			// };
			// return User.findByIdAndUpdate(userId, {$push: updateUser}).then((success) => {
			// 	if (success) {
			// 		console.log(success);
			// 		res.send('Order successfully checked out for user.')
			// 	} else {
			// 		res.send('Error in checking out order for user.')
			// 	}
			// })

			// let updateProduct = {
			// 	orders: [
			// 		{
			// 			orderId: orderId,
			// 			orderQty: ordQty
			// 		}
			// 	]
			// };
			// Product.findByIdAndUpdate(prodId, {$push: updateProduct}).then((success, error) => {
			// 	if (success) {
			// 		console.log('success');
			// 	} else {
			// 		console.log(error);
			// 	}
			// })
			// let newQty = Product.findById(prodId).then(result => {
			// 	return result.quantity;
			// })
			// console.log(newQty);
			// let updateQty = {
			// 	quantity: 
			// }
			// return Product.findByIdAndUpdate(prodId)

		}
		
	})

}

//[SECTION] Functionality [RETRIEVE]
// Retrieve ALL Orders
module.exports.getAllOrders = () => {
  return Order.find({}).then(result => {
	  return result;
	})

}
