//[SECTION] Dependencies and Modules
	const Product = require('../models/product');

//[SECTION] Functionality [CREATE] 
   module.exports.createProduct = (info) => { 
     let pName = info.productName;
     let pDesc = info.description;
     let pCost = info.price;
     let pQty = info.quantity;

     let newProduct = new Product({
     	productName: pName,
     	description: pDesc,
     	price: pCost,
     	quantity: pQty
     });

   	 return newProduct.save().then((savedProduct, error) => {
   	 	if (error) {
   	 		return 'Failed to Save New Product';
   	 	} else {
   	 		return savedProduct;
   	 	}
   		});
     
   }

//[SECTION] Functionality [RETRIEVE]
// Retrieve ALL Products
module.exports.getAllProducts = () => {
  return Product.find({}).then(result => {
	  return result;
	})

}

// Retrieve a SINGLE product
module.exports.getProduct = (id) => {
  return Product.findById(id).then(resultOfQuery => {
    return resultOfQuery;
  })

}

// Retrieve all ACTIVE products
module.exports.getAllActiveProducts = () => {
  return Product.find({isActive: true}).then(resultOfTheQuery => {
    return resultOfTheQuery;
  })

}

//[SECTION] Functionality [UPDATE]
// UPDATE product details
module.exports.updateProduct = (id, details) => {
  let pName = details.name;
  let pDesc = details.description;
  let pCost = details.price;
  let pQty = details.quantity; 

  let updatedProduct = {
    name: pName,
    description: pDesc,
    price: pCost,
    quantity: pQty
  }

  return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, error) => {
    if (error) {
      return 'Failed to Update Product';
    } else {
      return 'Product Successfully Updated';
    }
  })

}

// ARCHIVE product
module.exports.archiveProduct = (id) => {
  let update = {
    isActive: false
  }
  return Product.findByIdAndUpdate(id, update).then((archived, error) => {
    if (archived) {
      return `Product "${archived.productName}" has been archived.`;
    } else {
      return 'Failed to Archive Product';
    }
  })
}

// RESTORE product
module.exports.restoreProduct = (id) => {
  let update = {
    isActive: true 
  }
  return Product.findByIdAndUpdate(id, update).then((restored, error) => {
    if (restored) {
      return `Product "${restored.productName}" has been restored.`;
    } else {
      return 'Failed to Restore Product';
    }
  })

}


//[SECTION] Functionality [DELETE]
module.exports.deleteProduct = (id) => {
  return Product.findByIdAndRemove(id).then((removedProduct, err) => {
    if (err) {
      return 'Failed to Remove Product';
    } else {
      return 'Product Successfully Deleted.';
    }
  });

}


