// [SECTION] Dependencies and Modules
const auth = require("../auth");
const User = require('../models/user');
const bcrypt = require("bcrypt");
const dotenv = require("dotenv");

// [SECTION] Environment Setup
dotenv.config();
const salt = parseInt(process.env.SALT);

// [SECTION] Functionalities [CREATE]
module.exports.registerUser = (data) => {
	let firstName = data.firstName;
	let lastName = data.lastName;
	let email = data.email;
	let passW = data.password;

	let newUser = new User ({
		firstName: firstName,
  		lastName: lastName,
		email: email,
		password: bcrypt.hashSync(passW, salt),
		
	})

	return newUser.save().then((savedUser, error) => {
		if (savedUser) {
			return savedUser;
		} else {
			return res.send({message: 'Failed to register new account'});
		}

	})
	
}

// [SECTION] Functionalities [LOGIN]
module.exports.loginUser = (req, res) => {
	User.findOne({email: req.body.email}).then(foundUser => {
		if(foundUser === null){
			return res.send("User not Found");
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
		if(isPasswordCorrect){
			return res.send({accessToken: auth.createAccessToken(foundUser)})
		} else {
			return res.send("Incorrect Password")
		}
	}
	}).catch(err => res.send(err))

}

// [SECTION] SET USER AS ADMIN
module.exports.setUserAdmin = (req, res) => {
	let id = req.params.id;

	User.findById(id).then((result) => {
		if (result.isAdmin == true){
			res.send("User already has Admin Access.")
		} else {
			let update = {
				isAdmin: true
			}

			User.update(update).then((adminSet, error) => {
				if (adminSet) {
					res.send("User successfully set to Admin role.")
				} else {
					res.send("Error setting User as Admin. Try again.")
				}

			})
		}
	})

}

// [SECTION] SET ADMIN as USER
module.exports.setAdminUser = (req, res) => {
	let id = req.params.id;

	User.findById(id).then((result) => {
		if (result.isAdmin == false){
			res.send("User doesn't have Admin Access.")
		} else {
			let update = {
				isAdmin: false
			}

			User.update(update).then((userSet, error) => {
				if (userSet) {
					res.send("Admin successfully set to User role.")
				} else {
					res.send("Error setting Admin as User. Try again.")
				}

			})
		}
	})

}

//  [SECTION] GET USER DETAILS
module.exports.getUser = (req, res) => {
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
	
}

// [SECTION] Functionalities [RETRIEVE]
module.exports.getAllUsers = () => {
	return User.find({}).then((result) => {
		return result;
	})
}