// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true, 'Total Amount is Required']
	},
	purchasedOn: {
        type: Date,
        default: new Date()
    },
    customerId: {
    	type: String,
    	required: [true, 'Customer ID is Required']
    },
    products: [
    	{
    		productId: {
    			type: String,
    			required: [true, 'Product ID is required']
    		},
            productQuantity: {
                type: Number,
                required: [true, 'Product Quantity is required']
            }

    	}
    ]
})

// [SECTION] Model
const Order = mongoose.model('Order', orderSchema);
module.exports = Order;