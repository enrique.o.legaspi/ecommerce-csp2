// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, 'Product Name is Required']
	},
	description: {
		type: String,
		required: [true, 'Product Description is Required']
	},
	price: {
		type: Number,
		required: [true, 'Product Price is Required']
	},
	quantity: {
		type: Number,
		required: [true, 'Product Quantity is Required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
        type: Date,
        default: new Date()
    },
    orders: [
		{
			orderId: {
				type: String,
				required: [true, 'Order ID is required']
			},
			orderQty: {
				type: Number,
				required: [true, 'Order Quantity is Required']
			}
		}
	]
})

// [SECTION] Model
const Product = mongoose.model('Product', productSchema);
module.exports = Product;