// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
const userSchema = new mongoose.Schema({
	firstName: {
    		type: String,
    		required: [true, 'First Name is Required']
    },
    lastName: {
    		type: String,
    		required: [true, 'Last Name is Required']
    },
	email: {
		type: String,
		required: [true, 'Email is Required']
	},
	password: {
		type: String,
		required: [true, 'Password is Required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, 'Order ID is required']
			},
			orderTotal: {
				type: Number,
				required: [true, 'Order Total Amount is Required']
			}
		}
	]
});

// [SECTION] Model
const User = mongoose.model('User', userSchema);
module.exports = User;