//[SECTION] Dependencies and Modules
  const exp = require("express");
  const controller = require('../controllers/orders.js');
  const auth = require("../auth");

// destructure verify from auth
  const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router();

// Create Order
route.post('/makeOrder', verify, controller.makeOrder)

//[SECTION] [GET] Orders
route.get('/all', (req, res) => {
  controller.getAllOrders().then(outcome => {
    res.send(outcome);
  });
        
})

//[SECTION] Export Route System
  module.exports = route; 
