//[SECTION] Dependencies and Modules
const auth = require("../auth");
const exp = require("express");
const controller = require('./../controllers/products.js');

// destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] [POST] Routes
route.post('/addNew', verify, verifyAdmin, (req, res) => {
  let data = req.body; 
  controller.createProduct(data).then(outcome => {
    res.send(outcome); 
  });
})

//[SECTION] [GET] Routes
route.get('/all', (req, res) => {
  controller.getAllProducts().then(outcome => {
    res.send(outcome);
  });
        
})

route.get('/:id', (req, res) => {
  let prodId = req.params.id;
  controller.getProduct(prodId).then(result => {
      res.send(result);
  })

})

route.get('/', (req, res) => {
  controller.getAllActiveProducts().then(outcome => {
      res.send(outcome);
  })

})

// [SECTION] [PUT] Routes
// UPDATE Product
route.put('/:id/update', verify, verifyAdmin, (req, res) => {
    let id = req.params.id;
    let details = req.body;

    let pName = details.name;
    let pDesc = details.description;
    let pCost = details.price;
    let pQty = details.quantity;

    if (pName !== '' && pDesc !== '' && pCost !== '' && pQty !== '') {
      controller.updateProduct(id, details).then(outcome => {
        res.send(outcome);
      })
    } else {
      res.send('Incorrect Input, Make sure details are complete.');
    }

})

// ARCHIVE product
route.put('/:id/archive', verify, verifyAdmin, (req, res) => {
  let id = req.params.id;
  controller.archiveProduct(id).then(outcome => {
    res.send(outcome);
  })

})

// RESTORE product
route.put('/:id/restore', verify, verifyAdmin, (req, res) => {
  let id = req.params.id;
  controller.restoreProduct(id).then(outcome => {
    res.send(outcome);
  })

})

//[SECTION] [DEL] Routes
  route.delete('/:id', verify, verifyAdmin, (req, res) => {
    let id = req.params.id;
    controller.deleteProduct(id).then(outcome => {
      res.send(outcome);
    })
    
  })

//[SECTION] Export Route System
  module.exports = route; 