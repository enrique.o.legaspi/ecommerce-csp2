// [SECTION] Dependencies and Module
const exp = require("express");
const controller = require('./../controllers/users.js');
const auth = require("../auth");

// destructure verify from auth
const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const route = exp.Router();

//[SECTION] [POST]
route.post('/register', (req, res) => {
	let userDetails = req.body
	controller.registerUser(userDetails).then(outcome => {
		res.send(outcome);		
	})

})

// [SECTION] LOGIN
route.post("/login", controller.loginUser);

// [SECTION] GET USER DETAILS
route.get("/getUser", verify, controller.getUser)

// [SECTION] SET USER as ADMIN
route.put("/:id/setAdmin", verify, verifyAdmin, controller.setUserAdmin)

// [SECTION] SET ADMIN as USER
route.put("/:id/setUser", verify, verifyAdmin, controller.setAdminUser)

//[SECTION] [GET]
route.get('/all', (req, res) => {
	controller.getAllUsers().then(result => {
		res.send(result);
	})

})

// [SECTION] Routing - Expose Route System
module.exports = route;
